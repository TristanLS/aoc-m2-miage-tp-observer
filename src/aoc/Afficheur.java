package aoc;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Logger;

public class Afficheur implements ObserverdeCapteur {

	private Canal canal;
	private int numAfficheur = 0;
	private ArrayList<Integer> values = new ArrayList<Integer>();
	
	/**
	 * @param canal, Canal qui transmet les informations du Capteur à cet Afficheur
	 * @param numero, entier permettant d'identifier ce Canal
	 */
	public Afficheur(Canal canal, int numero) {
		this.canal = canal;
		this.numAfficheur = numero;
	}
	
	/**
	 * Quand l'état du capteur est modifié, les canaux (observeurs) sont notifiés 
	 * (Quand le canal est notifié, alors il notifie l'afficheur associé)
	 * Récupère la valeur à afficher depuis le Canal (avec un délai de transmission).
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public void update() throws InterruptedException, ExecutionException {
	    Future<Integer> futureValue = canal.getValue();
	    int value = futureValue.get();
	    Logger.getGlobal().info("L'afficheur " + numAfficheur + " a reçu la valeur : " + value);
	    if(!values.contains(value)) values.add(value);
	}
	
	/**
	 * Affiche la liste des valeurs enregistrées par l'Afficheur.
	 */
	public void displayValues() {
		for(int i = 0 ; i < values.size(); i++) {
			System.out.print(values.get(i) + " ");
		}
		System.out.println("");
	}
	


}
