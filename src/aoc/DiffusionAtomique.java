package aoc;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class DiffusionAtomique implements AlgoDiffusion{

	private ArrayList<Canal> canaux;
	private ArrayList<Canal> canauxAttendus;
	int value = 0;

	/**
	 * Configure l'algorithme en lui passant une liste de canaux (généralement celle du capteur)
	 * @param cannaux, la liste des canaux à associer à l'algorithme
	 */
	public void configure(ArrayList<Canal> canaux) {
		this.canaux = canaux;
	}
	
	/**
	 * Appelée par le Capteur. 
	 * Avec cet algorithme atomique, la valeur du capteur est incrémentée uniquement si le capteur n'est pas verrouillée.
	 * De plus, les canaux associés au Capteur sont ensuite notifiés que la valeur du capteur a changé en appelant la fonction notifyChange du Capteur.
	 * @param capteur, le capteur qui appelle cette fonction
	 * @param value, la valeur du capteur qui appelle cette fonction
	 */
	public void execute(Capteur capteur, int value) throws InterruptedException, ExecutionException {
		if(!capteur.isLocked()) {
			capteur.lock();
			canauxAttendus = new ArrayList<Canal>(canaux);
			capteur.setValue(value + 1);
			this.value = value + 1;
	        Logger.getGlobal().info("Diffusion Atomique | Capteur value : " + this.value);
			capteur.notifyChange();
		}
	}
	
	/**
	 * Appelée par le Capteur.
	 * Avec cet algorithme atomique, on attend que tous les canaux ont bien récupéré la valeur. Si c'est le cas, on dévérrouille le Capteur.
	 * @param canal, le Canal qui souhaite récupérer la valeur du Capteur
	 * @param capteur, le Capteur qui appelle cette fonction
	 * @return la valeur stockée
	 */
	public int readValue(Canal canal, Capteur capteur) {
		while(canauxAttendus.contains(canal)) {
			canauxAttendus.remove(canal);
		}
		if(canauxAttendus.size() == 0) {
			capteur.unlock();
		}
		return this.value;
	}
	
	
	
}
