package aoc;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Canal implements ObserverdeCapteur{
	
	private Capteur capteur;
	private Afficheur afficheur;
	private int numCanal = 0;
	private int delaiMin = 100;
	private int delaiMax = 2000;

	/**
	 * @param capteur, le Capteur observé par le Canal
	 * @param numero, un entier permettant d'identifier le Canal
	 */
	public Canal(Capteur capteur, int numero) {
		this.capteur = capteur;
		this.setNumCanal(numero);
	}
	
	/**
	 * @param capteur, le Capteur observé par le Canal
	 * @param afficheur, l'Afficheur qui est relié au Canal
	 * @param numero, un entier permettant d'identifier le Canal
	 */
	public Canal(Capteur capteur, Afficheur afficheur, int numero) {
		this.capteur = capteur;
		this.afficheur = afficheur;
		this.setNumCanal(numero);
	}
	
	/**
	 * @param capteur, le Capteur observé par le Canal
	 * @param afficheur, l'Afficheur qui est relié au Canal
	 * @param numero, un entier permettant d'identifier le Canal
	 * @param dMn, délai minimum de réception et d'envoi et de la valeur du Capteur (par défaut 100 ms)
	 * @param dMx, délai maximum de réception et d'envoi et de la valeur du Capteur (par défaut 2000 ms)
	 */
	public Canal(Capteur capteur, Afficheur afficheur, int numero, int dMn, int dMx) {
		this.capteur = capteur;
		this.afficheur = afficheur;
		this.setNumCanal(numero);
		this.delaiMin = dMn;
		this.delaiMax = dMx;
	}
	
	/**
	 * Setter de l'attribut "Afficheur"
	 * @param afficheur, l'afficheur à associer au Canal
	 */
	public void setAfficheur(Afficheur afficheur) {
		this.afficheur = afficheur;
	}
	
	/**
	 * Proxy qui effectue la liaison entre le capteur et l'afficheur
	 * Observer d'un Capteur
	 */
	
	/**
	 * Le Canal est un observeur de Capteur.
	 * Quand la valeur d'un Capteur est mise à jour, tous ses observeurs (canaux) sont notifiés, via l'appel à la fonction update des canaux.
	 * Notifie l'afficheur qu'il peut récupérer la valeur récupérée par le Canal.
	 * Un délai de transmission est ajouté.
	 */
	public void update() {
        ScheduledExecutorService scheduledService = Executors.newScheduledThreadPool(1);
        int delai = this.getDelay();
        Afficheur afficheur = this.afficheur;
        
		scheduledService.schedule(new Callable<Void>() {
            public Void call() throws InterruptedException, ExecutionException { afficheur.update(); return null; }
        }, delai, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Appelée par l'afficheur qui souhaite récupérer la valeur du capteur par le biais du canal.
	 * La valeur est transmise après un délai de transmission.
	 * @return la future valeur du capteur
	 */
	public Future<Integer> getValue() {
        ScheduledExecutorService scheduledService = Executors.newScheduledThreadPool(1);
        int delai = this.getDelay();
		Canal canal = this;
        
        Future<Integer> delayedValue = scheduledService.schedule(new Callable<Integer>() {
        	public Integer call() { return capteur.getValueAlgo(canal); }
        }, delai, TimeUnit.MILLISECONDS);
        		
        return delayedValue;
	}

	/**
	 * @return un délai de transmission, compris entre delaiMin et delaiMax.
	 */
	private int getDelay() {
		return new Random().nextInt(delaiMax-delaiMin) + delaiMin;
	}

	public int getNumCanal() {
		return numCanal;
	}

	public void setNumCanal(int numCanal) {
		this.numCanal = numCanal;
	}

}
