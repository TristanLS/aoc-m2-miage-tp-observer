package aoc;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class DiffusionEpoque implements AlgoDiffusion{
	
	int value = 0;
	
	public void configure(ArrayList<Canal> canaux) {}
	
	/**
	 * Appelée par le Capteur. 
	 * De plus, les canaux sont notifiés via l'appel à notifyChange du Capteur.
	 * @param capteur, le capteur qui appelle cette fonction
	 * @param value, la valeur du capteur qui appelle cette fonction
	 */
	public void execute(Capteur capteur, int value) throws InterruptedException, ExecutionException {
		capteur.setValue(value + 1);
		this.value = value + 1;
        Logger.getGlobal().info("Diffusion Epoque | Capteur value : " + this.value);
		capteur.notifyChange();
	}
	
	/**
	 * Appelée par la Capteur.
	 * Renvoie la valeur stockée par l'algorithme.
	 * @param canal, le Canal qui souhaite récupérer la valeur du Capteur
	 * @param capteur, le Capteur qui appelle cette fonction
	 * @return la valeur stockée
	 */
	public int readValue(Canal canal, Capteur capteur) {
		return this.value;
	}
}
