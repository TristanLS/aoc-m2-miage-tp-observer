package aoc;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public interface AlgoDiffusion {

	public void configure(ArrayList<Canal> canaux);
	public void execute(Capteur capteur, int value) throws InterruptedException, ExecutionException;
	public int readValue(Canal canal, Capteur capteur);
}
