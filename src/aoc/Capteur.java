package aoc;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface Capteur {

	public void attach(ObserverdeCapteur o); // Register un Observer
	public void detach(ObserverdeCapteur o); // Unregister un Observer
	public int getValueAlgo(Canal canal); 
	public int getCurrentValue();
	public void setValue(int value); 
	public void notifyChange() throws InterruptedException, ExecutionException; //Notifier les Observer d'un changement d'état
	public void lock();
	public void unlock();
	public boolean isLocked();
	public void tick() throws InterruptedException, ExecutionException;	
	public List<ObserverdeCapteur> getObservers();
	public void setAlgoDiffusion(AlgoDiffusion algo);
		
}

