package aoc;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

	public static void main(String[] args) {
		
		Capteur capteur = new CapteurImpl();
		
		ArrayList<Afficheur> afficheurs = new ArrayList<Afficheur>();
		ArrayList<Canal> canaux = new ArrayList<Canal>();
		
		for(int i = 0 ; i < 5 ; i++) {
			Canal canal = new Canal(capteur, i);
			Afficheur afficheur = new Afficheur(canal, i);
			canal.setAfficheur(afficheur);
			canaux.add(canal);
			afficheurs.add(afficheur);
			capteur.attach(canal);
		}
		
        int algoType = -1;
        Scanner scanner = new Scanner(System.in);
        while(algoType > 3 || algoType < 1) {
            System.out.println("Choisissez l'algorithme : 1 : atomique, 2 : séquentiel, ou 3 : par époque.");
            algoType = Integer.parseInt(scanner.nextLine());
        }

        AlgoDiffusion algo;
        if(algoType == 1) {
        	algo = new DiffusionAtomique();
        }else if(algoType == 2) {
        	algo = new DiffusionSeq();
        }else {
        	algo = new DiffusionEpoque();
        }
        algo.configure(canaux);

        capteur.setAlgoDiffusion(algo);

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        ScheduledExecutorService schedulerStop = Executors.newScheduledThreadPool(1);
		
        scheduler.scheduleWithFixedDelay(
                () -> {
					try {
						capteur.tick();
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
					}
				},0,1000,TimeUnit.MILLISECONDS
        );
        
        schedulerStop.schedule (() -> {scheduler.shutdown(); }, 20000, TimeUnit.MILLISECONDS);
        
        schedulerStop.schedule (() -> {
                    System.out.println("Liste des séquences : ") ; 
                    for(Afficheur afficheur : afficheurs) { afficheur.displayValues(); }
                }, 26000, TimeUnit.MILLISECONDS );
        

	}

}
