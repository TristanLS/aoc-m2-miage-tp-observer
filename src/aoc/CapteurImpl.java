package aoc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CapteurImpl implements Capteur{
	
	private ArrayList<ObserverdeCapteur> observers = new ArrayList<ObserverdeCapteur>();
	private AlgoDiffusion algoDiffusion;
	private int value = 0;
	private boolean isLocked = false;
	
	/**
	 * Lie un observeur (Canal) à ce Capteur.
	 */
	public void attach(ObserverdeCapteur observer) {
		observers.add(observer);
	}
	
	/**
	 * Délie un observeur (Canal) de ce Capteur.
	 */
	public void detach(ObserverdeCapteur observer) {
		observers.remove(observer);
	}
	
	/**
	 * Notifie tous les observeurs (canaux) que la valeur du Capteur a changé, en appelant leur méthode update.
	 */
	public void notifyChange() throws InterruptedException, ExecutionException {
		for(ObserverdeCapteur observer : observers) {
			observer.update();
		}
	}
	
	/**
	 * Renvoie la valeur stockée par l'algorithme lié.
	 * @param canal, le Canal qui demande à récupérer la valeur du Capteur.
	 * @return un entier, valeur stockée par l'algorithme.
	 */
	public int getValueAlgo(Canal canal) {
		return this.algoDiffusion.readValue(canal, this);
	}
	
	/**
	 * Fonction appelée à intervalles réguliers pour donner une nouvelle valeur au Capteur, en fonction de l'algorithme de diffusion.
	 */
	public void tick() throws InterruptedException, ExecutionException {
		this.algoDiffusion.execute(this, this.value);
	}
		
	/**
	 * Getter de l'attribut "value".
	 * @return value
	 */
	public int getCurrentValue() {
		return this.value;
	}
	
	/**
	 * Setter de l'attribut "value".
	 * @param value, un entier à donner.
	 */
	public void setValue(int value) {
		this.value = value;
	}
	

	/**
	 * L'attribut "isLocked" de ce Capteur passe à true. Il ne peut alors plus recevoir de nouvelle valeur.
	 */
	public void lock() {
		this.isLocked = true;
	}
	
	/**
	 * L'attribut "isLocked" de ce Capteur passe à false. Il peut recevoir une nouvelle valeur.
	 */
	public void unlock() {
		this.isLocked = false;
	}
	
	/**
	 * Getter de l'attribut "isLocked".
	 * @returns la valeur du booléen "isLocked", indiquant si le Capteur peut recevoir une nouvelle valeur.
	 */
	public boolean isLocked() {
		return this.isLocked;
	}
	
	/**
	 * Setter de l'attribut "algoDiffusion", l'algorithme qui détermine les changements de valeurs de Capteur.
	 * @param algo, une instance d'AlgoDiffusion (Atomique, par Séquence, ou par Époque).
	 */
	public void setAlgoDiffusion(AlgoDiffusion algo) {
		this.algoDiffusion = algo;
	}
	
	/**
	 * Getter de l'attribut "observers", la liste qui contient tous les observeurs (canaux) liés.
	 * @returns la liste des canaux liés à ce Capteur.
	 */
	public List<ObserverdeCapteur> getObservers(){
		return this.observers;
	}
	

}
