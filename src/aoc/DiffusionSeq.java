package aoc;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class DiffusionSeq implements AlgoDiffusion{

	private ArrayList<Canal> canaux;
	private ArrayList<Canal> canauxAttendus;
	int value = 0;
	
	/**
	 * Configure l'algorithme en lui passant une liste de canaux (généralement celle du capteur)
	 * @param cannaux, la liste des canaux à associer à l'algorithme
	 */
	public void configure(ArrayList<Canal> canaux) {
		this.canaux = canaux;
		this.canauxAttendus = new ArrayList<Canal>();
	}
	
	/**
	 * Appelée par le Capteur. 
	 * Avec cet algorithme séquentiel, la valeur du capteur est toujours incrémentée.
	 * De plus, si tous les canaux ont été notifié du précédent changement de valeur, alors les canaux sont notifiés via l'appel à notifyChange du Capteur.
	 * @param capteur, le capteur qui appelle cette fonction
	 * @param value, la valeur du capteur qui appelle cette fonction
	 */
	public void execute(Capteur capteur, int value) throws InterruptedException, ExecutionException {
		capteur.setValue(value + 1);
		if(canauxAttendus.size() == 0) {
			this.value = value + 1;
			canauxAttendus = new ArrayList<Canal>(canaux);
	        Logger.getGlobal().info("Diffusion Seq | Capteur value : " + this.value);
			capteur.notifyChange();
		}
	}
	
	/**
	 * Renvoie la valeur stockée par l'algorithme.
	 * @param canal, le Canal qui souhaite récupérer la valeur du Capteur
	 * @param capteur, le Capteur qui appelle cette fonction
	 * @return la valeur stockée.
	 */
	public int readValue(Canal canal, Capteur capteur) {
		while(canauxAttendus.contains(canal)) {
			canauxAttendus.remove(canal);
		}
		return this.value;
	}
}
