package aoc;

import java.util.concurrent.ExecutionException;

//client

public interface ObserverdeCapteur {

	public void update() throws InterruptedException, ExecutionException; //quand notifié quand le subject a changé son état
}
